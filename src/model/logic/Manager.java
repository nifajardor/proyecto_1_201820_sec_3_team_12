package model.logic;

import API.IManager;
import model.data_structures.ArregloFlexible;
import model.data_structures.DoubleLinkedList;
import model.data_structures.ICola;
import model.data_structures.ILista;
import model.data_structures.IPila;
import model.vo.Bike;
import model.vo.Station;
import model.vo.Trip;
import model.data_structures.Node;
import model.data_structures.Stack;
import model.vo.*;
import com.opencsv.CSVReader;
import java.io.FileReader;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class Manager implements IManager {
	//Ruta del archivo de datos 2017-Q1
	// TODO Actualizar
	public static final String TRIPS_Q1 = "data/Divvy_Trips_2017_Q1.csv";

	//Ruta del archivo de trips 2017-Q2
	// TODO Actualizar	
	public static final String TRIPS_Q2 = "data/Divvy_Trips_2017_Q2.csv";

	//Ruta del archivo de trips 2017-Q3
	// TODO Actualizar	
	public static final String TRIPS_Q3 = "data/Divvy_Trips_2017_Q3.csv";

	//Ruta del archivo de trips 2017-Q4
	// TODO Actualizar	
	public static final String TRIPS_Q4 = "data/Divvy_Trips_2017_Q4.csv";

	//Ruta del archivo de trips 2017-Q1-Q4
	// TODO Actualizar	
	public static final String TRIPS_Q1_Q4 = "Ruta Trips 2017-Q1-Q4 en directorio data";

	//Ruta del archivo de stations 2017-Q1-Q2
	// TODO Actualizar	
	public static final String STATIONS_Q1_Q2 = "data/Divvy_Stations_2017_Q1Q2.csv";

	//Ruta del archivo de stations 2017-Q3-Q4
	// TODO Actualizar	
	public static final String STATIONS_Q3_Q4 = "data/Divvy_Stations_2017_Q3Q4.csv";
	//Formato de la fecha
	public static final String DATE_PATTERN = "MM/dd/yyyy HH:mm:ss";

	public static final int RADIO_TIERRA = 6371;



	private Queue tripQueue;
	private Queue statQueue;
	private ArregloFlexible<Trip> arregloTrips;
	private ArregloFlexible<Station> arregloStats;
	private ArregloFlexible<Station> arregloBikes;

	private Ordenamientos ordenamiento = new Ordenamientos();

	public ICola<Node> A1ViajesEnPeriodoDeTiempo(LocalDateTime fechaInicial, LocalDateTime fechaFinal) {


		Trip tActual;
		ICola<Node> lista = new Queue();
		for(int i = 0;i<arregloTrips.darTamAct();i++) {
			tActual = (Trip) arregloTrips.darElementoI(i);
			if(tActual.getStartTime().compareTo(fechaInicial) >0 && tActual.getStopTime().compareTo(fechaFinal)<0) {
				Node n = new Node(tActual);
				lista.enqueue(n);
			}
		}
		return lista;

	}

	public double darLatitudPorId(int id) {
		double aRetornar = 0;
		for(int i = 0; i<arregloStats.darTamAct();i++) {
			Station actual = (Station) arregloStats.darElementoI(i);
			if(actual.getStationId() == id) {
				aRetornar = actual.getLat();
			}
		}
		return aRetornar;
	}

	public double darLongitudPorId(int id) {
		double aRetornar = 0;
		for(int i = 0; i<arregloStats.darTamAct();i++) {
			Station actual = (Station) arregloStats.darElementoI(i);
			if(actual.getStationId() == id) {
				aRetornar = actual.getLong();
			}
		}
		return aRetornar;
	}
	public ICola<Node> A2BicicletasOrdenadasPorNumeroViajes(LocalDateTime fechaInicial, LocalDateTime fechaFinal) {
		Trip tActual;
		ArregloFlexible<Bike> arreglo = new ArregloFlexible<Bike>(arregloTrips.darTamAct());
		for(int i = 0;i<arregloTrips.darTamAct();i++) {
			tActual = (Trip) arregloTrips.darElementoI(i);
			if(tActual.getStartTime().isAfter(fechaInicial) && tActual.getStopTime().isBefore(fechaFinal)) {
				int bikeId = tActual.getBikeId();
				int duracionViaje = tActual.getTripDuration();
				double lat1 = darLatitudPorId(tActual.getStartStationId());
				double long1 = darLongitudPorId(tActual.getStartStationId());
				double lat2 = darLatitudPorId(tActual.getEndStationId());
				double long2 = darLongitudPorId(tActual.getEndStationId());
				double distancia = distancia(lat1, long1, lat2, long2);
				boolean existe = false;
				//System.out.println(tActual.toString());
				for(int j = 0;j<arreglo.darTamAct()&&!existe;j++) {
					Bike act = (Bike) arreglo.darElementoI(j);
					if(act.getBikeId() == bikeId) {
						act.agregarViaje();
						act.agregarDuracion(duracionViaje);
						act.agregarDistancia(distancia);
						existe = true;
					}
					//System.out.println(act.toString());
				}


				if(!existe) {
					Bike agregar = new Bike(bikeId, 1, distancia, duracionViaje);
					arreglo.agregarElem(agregar);
					//System.out.println(agregar.toString());
				}
			}

		}
		arreglo.mergeSort();

		ICola<Node> aRet = new Queue();
		for(int i = 0;i<arreglo.darTamAct();i++) {
			Node n = new Node(arreglo.darElementoI(i));
			aRet.enqueue(n);
			Bike act = (Bike) arreglo.darElementoI(i);
			System.out.println(act.toString());
		}
		return aRet;
	}

	public ICola<Node> A3ViajesPorBicicletaPeriodoTiempo(int bikeId, LocalDateTime fechaInicial, LocalDateTime fechaFinal) {
		Trip tActual;

		ICola<Node> arreglo = new Queue();

		for(int i = (arregloTrips.darTamAct());i>0;i--) {
			tActual = (Trip) arregloTrips.darElementoI(i-1);
			System.out.println(tActual.getBikeId() + "");
			if(bikeId == tActual.getBikeId()) {
				if(tActual.getStartTime().compareTo(fechaInicial) >0 && tActual.getStopTime().compareTo(fechaFinal)<0) {
					Node n = new Node(tActual);
					arreglo.enqueue(n);
				}
			}
		}
		return arreglo;
	}

	public ICola<Node> A4ViajesPorEstacionFinal(int endStationId, LocalDateTime fechaInicial, LocalDateTime fechaFinal) {
		Trip tActual;
		ICola<Node> arreglo = new Queue();
		for(int i = (arregloTrips.darTamAct());i>0;i--) {
			tActual = (Trip) arregloTrips.darElementoI(i-1);
			if(endStationId == tActual.getEndStationId()) {
				if(tActual.getStartTime().compareTo(fechaInicial) >0 && tActual.getStopTime().compareTo(fechaFinal)<0) {
					Node n = new Node(tActual);
					arreglo.enqueue(n);
				}
			}
		}
		return arreglo;
	}

	public ICola<Station> B1EstacionesPorFechaInicioOperacion(LocalDateTime fechaComienzo) {

		ICola<Station>cola=new Queue();

		for(int i =0;i<arregloStats.darTamAct();i++){

			if(((Station)arregloStats.darElementoI(i)).getStartDate().isAfter(fechaComienzo)){

				cola.enqueue((Station)arregloStats.elementos[i]);
			}
		}


		return cola;
	}

	public ILista<Bike> B2BicicletasOrdenadasPorDistancia(LocalDateTime fechaInicial, LocalDateTime fechaFinal) {
		return null;
	}

	public ILista<Trip> B3ViajesPorBicicletaDuracion(int bikeId, int tiempoMaximo, String genero) {
		ordenamiento.shellsort(arregloTrips);
		ILista<Trip> lista=new DoubleLinkedList<>();
		for(int i =0;i<arregloTrips.darTamAct();i++){
			if(((Trip)arregloTrips.darElementoI(i)).getBikeId()==bikeId&&((Trip)arregloTrips.darElementoI(i)).getTripDuration()<tiempoMaximo&&((Trip)arregloTrips.darElementoI(i)).getGender().equals(genero)){
				lista.add(((Trip)arregloTrips.darElementoI(i)));
			}
		}
		return lista;
	}

	public ILista<Trip> B4ViajesPorEstacionInicial(int startStationId, LocalDateTime fechaInicial, LocalDateTime fechaFinal) {
		ordenamiento.shellsort(arregloTrips);
		ILista<Trip> lista=new DoubleLinkedList<>();
		for(int i =0;i<arregloTrips.darTamAct();i++){
			if(((Trip)arregloTrips.darElementoI(i)).getStartStationId()==startStationId&&((Trip)arregloTrips.darElementoI(i)).getStartTime().isAfter(fechaInicial)&&((Trip)arregloTrips.darElementoI(i)).getStartTime().isBefore(fechaFinal)){
				lista.add(((Trip)arregloTrips.darElementoI(i)));
			}
		}
		return lista;

	}


	public void C1cargar(String rutaTrips, String rutaStations) {
		try {
			CSVReader lectorTrips = new CSVReader(new FileReader(rutaTrips));
			CSVReader lectorStations = new CSVReader(new FileReader(rutaStations));
			String[] trip;
			String[] stat;
			trip = lectorTrips.readNext();
			stat = lectorStations.readNext();
			arregloStats = new ArregloFlexible<Station>(600);
			arregloTrips = new ArregloFlexible<Trip>(1800000);
			tripQueue = new Queue();
			statQueue = new Queue();
			DateTimeFormatter formato = DateTimeFormatter.ofPattern(DATE_PATTERN);
			int cont = 0;
			while((stat = lectorStations.readNext()) != null) {
				int id = Integer.parseInt(stat[0]);
				String statName = stat[1];
				String[]fecha=stat[6].split("(?=\\s)");
				double lat = Double.parseDouble(stat[3]);
				double lon = Double.parseDouble(stat[4]);
				LocalDateTime startDate = convertirFecha_Hora_LDT(fecha[0], fecha[1]);
				Station estacion = new Station(id, statName, startDate, lat, lon);
				String info = estacion.toString();
				Node nodo = new Node<>(estacion);

				//statQueue.enqueue(nodo);
				arregloStats.agregarElem(estacion);

				cont++;
			}
			int cont2 = 0;
			while((trip = lectorTrips.readNext()) != null) {
				int tripID = Integer.parseInt(trip[0]);
				String[] fecha=trip[1].split("(?=\\s)");
				String[] fecha1=trip[2].split("(?=\\s)");
				LocalDateTime startTime = convertirFecha_Hora_LDT(fecha[0], fecha[1]);
				LocalDateTime endTime = convertirFecha_Hora_LDT(fecha1[0], fecha1[1]);
				int bikeID = Integer.parseInt(trip[3]);
				int tripDuration = Integer.parseInt(trip[4]);
				int startStatID = Integer.parseInt(trip[5]);
				int endStatID = Integer.parseInt(trip[7]);
				String gender;
				if(trip[10].equals(Trip.MALE))
					gender = Trip.MALE;
				else if(trip[10].equals(Trip.FEMALE))
					gender = Trip.FEMALE;
				else
					gender = Trip.UNKNOWN;

				Trip viaje = new Trip(tripID, startTime, endTime, bikeID, tripDuration, startStatID, endStatID, gender);
				Node nodo = new Node<>(viaje);
				//tripQueue.enqueue(nodo);
				arregloTrips.agregarElem(viaje);

				cont2++;
			}
			System.out.println("Total trips cargados en el sistema: " + cont2);
			System.out.println("Total estaciones cargadas en el sistema: "+ cont);

		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}


	}

	public ICola<Trip> C2ViajesValidadosBicicleta(int bikeId, LocalDateTime fechaInicial, LocalDateTime fechaFinal) {
		Stack<Trip> lista=new Stack();
		ArregloFlexible<Trip> lista2=new ArregloFlexible<>(1000000);
		ICola<Trip> inconsistencias=new Queue();
		for(int i=0;i<arregloTrips.darTamAct();i++){
			if(((Trip)arregloTrips.darElementoI(i)).getBikeId()==bikeId&&((Trip)arregloTrips.darElementoI(i)).getStartTime().isAfter(fechaInicial)&&((Trip)arregloTrips.darElementoI(i)).getStartTime().isBefore(fechaFinal)){
				lista2.agregarElem(((Trip)arregloTrips.darElementoI(i)));
			}
		}
		int contador=0;
		for(int i=0;i<lista2.darTamAct();i++){
			if(lista.isEmpty()){
				lista.push(lista2.darElementoI(i));

			}else{
				if(((Trip)lista2.darElementoI(i)).getStartStationId()==((Trip)lista.get(contador)).getStartStationId()){
					lista.push(((Trip)lista2.darElementoI(i)));
					contador++;
				}else{
					inconsistencias.enqueue(lista.pop());
					inconsistencias.enqueue(((Trip)lista2.darElementoI(i)));
				}
			}
		}
		for(Object trip:lista){
			lista.pop();
		}
		return inconsistencias;
	}

	public ILista<Bike> C3BicicletasMasUsadas(int topBicicletas) {
		ILista<Bike> lista=new DoubleLinkedList<>();
		arregloBikes=new ArregloFlexible<>(1000000);
		for(int i =0;i<arregloTrips.darTamAct();i++){
			int duracion=0;
			int viajes=0;
			boolean existe=false;
			if(arregloBikes.darTamAct()!=0){
				for(int j=0;j<arregloBikes.darTamAct();j++){
					if(((Trip)arregloTrips.darElementoI(i)).getBikeId()==((Bike)arregloBikes.darElementoI(j)).getBikeId()){
						((Bike)arregloBikes.darElementoI(j)).agregarViaje();
						((Bike)arregloBikes.darElementoI(j)).aumentarDuracion(((Trip)arregloTrips.darElementoI(i)).getTripDuration());
						((Bike)arregloBikes.darElementoI(j)).aumentarDistancia(distanciaViaje(((Trip)arregloTrips.darElementoI(i))));
						existe=true;
					}
				}

			}
			if (existe==false){
				arregloBikes.agregarElem(new Bike(((Trip)arregloTrips.darElementoI(i)).getBikeId(), 1, distanciaViaje(((Trip)arregloTrips.darElementoI(i))), ((Trip)arregloTrips.darElementoI(i)).getTripDuration()));


			}
		}
		ordenamiento.shellsortBikes(arregloBikes, 2);
		for(int c=0;c<=topBicicletas;c++){
			lista.add(((Bike)arregloBikes.darElementoI(c)));
		}

		return lista;
	}

	public ICola<Node> C4ViajesEstacion(int StationId, LocalDateTime fechaInicial, LocalDateTime fechaFinal) {
		Trip tActual;
		ICola<Node> lista = new Queue();
		ArregloFlexible<Trip> arreglo = new ArregloFlexible<>(arregloTrips.darTamAct());
		for(int i = (arregloTrips.darTamAct());i>0;i--) {
			tActual = (Trip) arregloTrips.darElementoI(i-1);
			if(tActual.getStartTime().compareTo(fechaInicial) >0 && tActual.getStopTime().compareTo(fechaFinal)<0) {
				if(tActual.getStartStationId() == StationId) {
					Trip agregar = new Trip(tActual.getTripId(), tActual.getBikeId(), Trip.INICIO, tActual.getStartTime());
					arreglo.agregarElem(agregar);
					System.out.print("Trip Id: "+ tActual.getTripId() + ", ");
					System.out.print("Bike Id: "+ tActual.getBikeId() + ", ");
					System.out.print("El viaje es de inicio. Hora inicio: " + tActual.getStartTime() + '\n');
					System.out.println("-----");
				}
				if(tActual.getEndStationId() == StationId) {
					Trip agregar = new Trip(tActual.getTripId(), tActual.getBikeId(), Trip.TERMINACION, tActual.getStopTime());
					arreglo.agregarElem(agregar);
					System.out.print("Trip Id: "+ tActual.getTripId() + ", ");
					System.out.print("Bike Id: "+ tActual.getBikeId() + ", ");
					System.out.print("El viaje es de terminación. Hora terminación: " + tActual.getStopTime() + '\n');
					System.out.println("-----");
				}
			}
		}
		arreglo.mergeSortC4();
		for(int i =0;i<arreglo.darTamAct();i++) {
			Node n = new Node(arreglo.darElementoI(i));
			lista.enqueue(n);
		}
		return lista;
	}
	private static LocalDateTime convertirFecha_Hora_LDT(String fecha, String hora)
	{
		String[] datosFecha = fecha.split("/");
		String[] datosHora = hora.split(":");

		int agno = Integer.parseInt(datosFecha[2]);
		int mes = Integer.parseInt(datosFecha[0]);
		int dia = Integer.parseInt(datosFecha[1]);
		int horas = Integer.parseInt(datosHora[0].trim());
		int minutos = Integer.parseInt(datosHora[1]);
		int segundos = 0;

		if(datosHora.length>2){
			segundos=Integer.parseInt(datosHora[2]);
		}

		return LocalDateTime.of(agno, mes, dia, horas, minutos, segundos);
	}


	public double distanciaViaje(Trip trip){
		double longitudStart=0;
		double longitudEnd=0;
		double latitudStart=0;
		double latitudEnd=0;
		double distancia=0;
		for(int i=0;i<arregloStats.darTamAct();i++){
			if(((Station)arregloStats.darElementoI(i)).getStationId()==trip.getStartStationId()){
				latitudStart=((Station)arregloStats.darElementoI(i)).getLat();
				longitudStart=((Station)arregloStats.darElementoI(i)).getLong();
			}
			if(((Station)arregloStats.darElementoI(i)).getStationId()==trip.getEndStationId()){
				latitudEnd=((Station)arregloStats.darElementoI(i)).getLat();
				longitudEnd=((Station)arregloStats.darElementoI(i)).getLong();
			}
		}
		distancia=distancia(latitudStart, longitudStart, latitudEnd, longitudEnd);
		return distancia;
	}


	public double distancia(double lat1, double long1, double lat2, double long2) {
		double deltaLat = Math.toRadians((lat2-lat1));
		double deltaLong = Math.toRadians((long2-long1));

		lat1 = Math.toRadians(lat1);
		lat2 = Math.toRadians(lat2);
		double a = harvesin(deltaLat) + Math.cos(lat1)*Math.cos(lat2)*harvesin(deltaLong);
		double c = 2*Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
		return 1000*RADIO_TIERRA*c;
	}
	public double harvesin(double valor) {
		return Math.pow(Math.sin(valor / 2), 2);
	}

	//	public void mergeSort(ArregloFlexible<Node> aOrdenar, int tipoOrden, int tipoElem) {
	//		ArregloFlexible<Node> aux = new ArregloFlexible<Node>(aOrdenar.darTamAct());
	//		dividir(aOrdenar, aux, 0, aOrdenar.darTamAct()-1, tipoOrden, tipoElem);
	//	}
	//	public void dividir(ArregloFlexible<Node> aOrdenar, ArregloFlexible<Node> aux, int menor, int mayor, int tipoOrden, int tipoElem) {
	//		if(mayor<=menor) return;
	//		int medio = menor + (mayor-menor)/2;
	//		dividir(aOrdenar, aux, menor, medio, tipoOrden, tipoElem);
	//		dividir(aOrdenar, aux, medio+1, mayor, tipoOrden, tipoElem);
	//		juntar(aOrdenar, aux, menor, medio, mayor, tipoOrden, tipoElem);
	//	}
	//	public void juntar(ArregloFlexible<Node> aOrdenar, ArregloFlexible<Node> aux, int menor, int medio, int mayor, int tipoOrden, int tipoElem) {
	//		for(int i = menor;i<=mayor;i++) {
	//			aux.anadirElementoPos(i, aOrdenar.darElementoI(i));
	//		}
	//		int i = menor;
	//		int j = medio +1;
	//		for(int k = menor;k<= mayor;k++) {
	//			if(i>medio) {
	//				aOrdenar.anadirElementoPos(k, aux.darElementoI(j++));
	//			}
	//			else if(j>mayor) {
	//				aOrdenar.anadirElementoPos(k, aux.darElementoI(i++));
	//			}
	//			else if(aux.darElementoI(j).compareTo(aux.darElementoI(i).darElemento(), tipoElem, tipoOrden) <0) {
	//				aOrdenar.anadirElementoPos(k, aux.darElementoI(j++));
	//			}
	//			else {
	//				aOrdenar.anadirElementoPos(k, aux.darElementoI(i++));
	//			}
	//		}
	//	}
}
