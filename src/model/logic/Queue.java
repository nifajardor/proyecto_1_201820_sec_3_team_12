package model.logic;
import java.util.Iterator;

import model.data_structures.*;
public class Queue<T> extends DoubleLinkedList implements ICola{

	public Queue() {
		super();
	}
	@Override
	public void enqueue(Object elem) {
		// TODO Auto-generated method stub
		anadirFinal((Node) elem);
	}

	@Override
	public Object dequeue() {
		// TODO Auto-generated method stub
		
		return eliminarPrimero();
	}

	@Override
	public int getSize() {
		// TODO Auto-generated method stub
		return super.darTamanio();
	}

	@Override
	public boolean isEmpty() {
		// TODO Auto-generated method stub
		
		return (tamanio==0? true : false);
	}

	@Override
	public Iterator<T> iterator() {
		// TODO Auto-generated method stub
		return new IteradorLista<T>( primero);
	}

}
