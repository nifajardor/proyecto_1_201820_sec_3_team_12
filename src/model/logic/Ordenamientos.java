package model.logic;

import model.data_structures.ArregloFlexible;
import model.data_structures.ICola;
import model.data_structures.Node;
import model.vo.Station;
import model.vo.Bike;
public class Ordenamientos<E> {



	public <T> void shellsort(ArregloFlexible cola){

		int	n=cola.darTamano();

		
		long inicio=System.currentTimeMillis();

			 for (int gap = n/2; gap > 0; gap /= 2)
			    {
			      
			        for (int i = gap; i < n; i += 1)
			        {
			           
			            Comparable temp = cola.elementos[i];
			 
			            // shift earlier gap-sorted elements up until the correct 
			            // location for a[i] is found
			            int j;            
			            for (j = i; j >= gap && cola.elementos[j-gap].compareTo(temp)<0 ; j -= gap){
			            	
			               cola.elementos[j]= cola.elementos[j-gap];
			              
			            }
			            cola.elementos[j]=temp;
			            //  put temp (the original a[i]) in its correct location
			            
			        }
			    }

		}
	
	public void mergeSort(ArregloFlexible<Node> aOrdenar, int tipoOrden, int tipoElem) {
		ArregloFlexible<Node> aux = new ArregloFlexible<Node>(aOrdenar.darTamAct());
		dividir(aOrdenar, aux, 0, aOrdenar.darTamAct()-1, tipoOrden, tipoElem);
	}
	public void dividir(ArregloFlexible<Node> aOrdenar, ArregloFlexible<Node> aux, int menor, int mayor, int tipoOrden, int tipoElem) {
		if(mayor<=menor) return;
		int medio = menor + (mayor-menor)/2;
		dividir(aOrdenar, aux, menor, medio, tipoOrden, tipoElem);
		dividir(aOrdenar, aux, medio+1, mayor, tipoOrden, tipoElem);
		
	}
	public <T> void shellsortBikes(ArregloFlexible cola,int tipo){

		int	n=cola.darTamano();


		long inicio=System.currentTimeMillis();

			 for (int gap = n/2; gap > 0; gap /= 2)
			    {
			      
			        for (int i = gap; i < n; i += 1)
			        {
			           
			            Comparable temp = cola.elementos[i];
			 
			            // shift earlier gap-sorted elements up until the correct 
			            // location for a[i] is found
			            int j;            
			            for (j = i; j >= gap && ((Bike)cola.elementos[j-gap]).compareTo(((Bike)temp),tipo)<0 ; j -= gap){
			            	
			               cola.elementos[j]= cola.elementos[j-gap];
			              
			            }
			            cola.elementos[j]=temp;
			            //  put temp (the original a[i]) in its correct location
			            
			        }
			    }

		}

}
