package model.data_structures;

import java.util.Iterator;


public class DoubleLinkedList<T> implements ILista{
	protected Node primero;
	protected Node ultimo;
	protected int tamanio;
	
	public Node darPrimero() {
		return primero;
	}
	public Node darUltimo() {
		return ultimo;
	}
	public int darTamanio() {
		return tamanio;
	}
	public <T>DoubleLinkedList() {
		primero = null;
		ultimo = null;
		tamanio = 0;
	}
	public Node eliminarPrimero() {
		Node eliminado = null;
		if(primero != null) {
			eliminado = primero;
			if(primero == ultimo) {
				primero = null;
				ultimo = null;
			}
			else {
				primero = primero.darSig();
				primero.cambiarAnt(null);
			}
			tamanio--;
		}		
		return eliminado;
	}
	
	public Node eliminarUltimo() {
		Node eliminado = null;
		if(primero!=null) {
			eliminado = ultimo;
			if(primero==ultimo) {
				primero = null;
				ultimo=null;
			}
			else {
				ultimo=ultimo.darAnt();
				ultimo.cambiarSig(null);
			}
			tamanio--;
		}
		return eliminado;
	}
	public void anadirInicio(Node pNodo) {
		if(primero == null) {
			primero = pNodo;
			ultimo = pNodo;
		}
		else {
			pNodo.cambiarSig(primero);
			primero.cambiarAnt(pNodo);
			primero = pNodo;
		}
		tamanio++;
	}
	public void anadirFinal(Node pNodo) {
		if(primero == null) {
			primero = pNodo;
			ultimo = pNodo;
		}
		else {
			ultimo.cambiarSig(pNodo);
			pNodo.cambiarAnt(ultimo);
			ultimo = pNodo;
		}
		tamanio++;
	}
	

	@Override
	public Comparable remove(Comparable elem) {
		Node actual=primero;
		if(primero!=null){
			if(primero.darElemento().equals(elem)){
				actual= actual.darSig();
				primero=actual;
				tamanio--;
				return true;
			}
			else{
				while(actual.darSig()!=null){
					if(actual.darSig().darElemento().equals(elem)){
						actual.cambiarSig(actual.darSig().darSig());
						if(actual.darSig().darSig()!=null){
							 actual.darSig().darSig().cambiarAnt(actual);
						}
						tamanio--;
						return true;
					}
					actual= actual.darSig();
				}
			}

		}
		return false;
	}
	@Override
	public int size() {
		// TODO Auto-generated method stub
		return tamanio;
	}
	@Override
	public Comparable get(Comparable elem) {
		// TODO Auto-generated method stub
		Node actual=primero;
		while(actual!=null){
			if(actual.darElemento().equals(elem)){
				return actual.darElemento();
			}
			actual=actual.darSig();
		}
		return null;
	}
	@Override
	public Comparable get(int pos) {
		// TODO Auto-generated method stub
		int contador=0;
		Comparable elemento=null;
		Node actual=primero;
		if(pos<0){
			throw new IndexOutOfBoundsException();
		}
		while(actual!=null){
			if(contador==pos){
				elemento=actual.darElemento();
				return elemento;
			}
			actual=actual.darSig();
			contador++;
		}
		if(elemento==null){
			throw new IndexOutOfBoundsException();
		}
		return elemento;
	}
	@Override
	public boolean isEmpty() {
		// TODO Auto-generated method stub
		if(tamanio==0){
			return true;
		}else{
			return false;
		}
	}
	@Override
	public Iterator iterator() {
		// TODO Auto-generated method stub
		return new IteradorLista<T>( primero);
	}
	@Override 
	public void add(Comparable elem){
		Node pNodo=new Node(elem);
		if(primero == null) {
			primero = pNodo;
			ultimo = pNodo;
		}
		else {
			pNodo.cambiarSig(primero);
			primero.cambiarAnt(pNodo);
			primero = pNodo;
		}
		tamanio++;
	}
	public void set(int index, Comparable element) throws IndexOutOfBoundsException 
	{
		// TODO Completar seg�n la documentaci�n

		int contador=1;
		Comparable retirado=null;
		Node actual=primero;
		if(index<0){
			throw new IndexOutOfBoundsException();
		}
		if(primero==null){

		}else if(index==0){
			retirado=primero.darElemento();
			primero.cambiarElem(element);
			
		}else{
			actual=actual.darSig();
			while(actual!=null){
				if(contador==index){
					retirado=actual.darElemento();
					actual.cambiarElem(element);
					
				}
				actual=actual.darSig();
				contador++;
			}
		}
		if(retirado==null){
			throw new IndexOutOfBoundsException();
		}

	}
}
