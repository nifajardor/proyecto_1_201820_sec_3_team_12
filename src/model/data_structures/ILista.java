package model.data_structures;

import java.util.Iterator;

public interface ILista<T extends Comparable<T>> extends Iterable<T> {

	public void add(T elem);

	public T remove(T elem);

	public int size();

	public T get(T elem);

	public T get(int pos);
	
	public boolean isEmpty();

    @Override
    public Iterator<T> iterator();

	
}
