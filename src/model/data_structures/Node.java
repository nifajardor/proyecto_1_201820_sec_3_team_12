package model.data_structures;

import model.vo.Bike;

public class Node<E extends Comparable<E>>  {
	public static final int ESTACION = 0;
	public static final int VIAJE = 1;
	public static final int BICI = 2;
	private Node<E> siguiente;
	private Node<E> anterior;
	private Comparable elemento;

	public Node(Comparable pElem) {
		elemento = pElem;
		anterior = null;
		siguiente = null;
	}

	public Comparable darElemento() {
		return elemento;
	}
	public Node<E> darSig() {
		return siguiente;
	}
	public Node<E> darAnt() {
		return anterior;
	}
	public void cambiarSig(Node<E> pSig) {
		siguiente = pSig;
	}
	public void cambiarAnt(Node<E> pAnt) {
		anterior = pAnt;
	}
	public void cambiarElem(Comparable elem) {
		elemento=elem;
	}
	public void imprimirr() {
		System.out.println(elemento.toString());
	}
	public int compareTo(Comparable elem, int tipoElem, int ordenamiento) {
		int retorno = 0;
		if(tipoElem == BICI) {
			Bike esta = (Bike) elemento;
			Bike otra = (Bike) elem;
			retorno = esta.compareTo(otra, ordenamiento);
		}
		else  {
			retorno = elemento.compareTo(elem);
		}
		return retorno;
	}


	public Node eliminar() {

		if(siguiente !=null && anterior == null) {
			siguiente.cambiarAnt(null);
		}
		else if(siguiente == null && anterior != null) {
			anterior.cambiarSig(null);
		}
		else {
			siguiente.cambiarAnt(anterior);
			anterior.cambiarSig(siguiente);
		}
		return siguiente;
	}





}