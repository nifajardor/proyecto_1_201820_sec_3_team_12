package model.data_structures;

import java.util.Iterator;

public class Stack<T> extends DoubleLinkedList<T>   {

	public Stack() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	public boolean isEmpty() {
		// TODO Auto-generated method stub
		if(tamanio==0){
			return true;
		}
	return false;
	}

	@Override
	public int size() {
		// TODO Auto-generated method stub
		return tamanio;
	}
	public Node darPrimero(){
		return primero;
	}
	public Node darUltimo(){
		return ultimo;
	}

	
	public void push(Object t) {
		// TODO Auto-generated method stub
		anadirInicio(new Node((Comparable) t));
	}

	
	public T pop() {
		// TODO Auto-generated method stub
		return (T) eliminarPrimero().darElemento();
	}
	
	
	public Node eliminarNodo(Node pNode) {
		Node ret = null;
		if(pNode == primero && pNode == ultimo) {
			primero = null;
			ultimo = null;
		}
		else {
			ret =	pNode.eliminar();
		}
		return ret;
	}
	
	

	
	public Node eliminarPrim() {
		// TODO Auto-generated method stub
		Node ret = null;
		primero = primero.darSig();
		if(primero != null) {
			primero.cambiarAnt(null);
			ret = primero;
		}
		return ret;
	}

	


	

}
