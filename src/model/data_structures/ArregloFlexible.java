package model.data_structures;

import model.vo.Bike;
import model.vo.Trip;

public class ArregloFlexible<T> {

	public static final int START = 0;
	public static final int STOP = 1;
	private int tamanoMax;
	private int tamanoAct;
	public Comparable elementos[];

	public ArregloFlexible(int max){
		elementos=(Comparable[])new Comparable[max];
		tamanoMax=max;
		tamanoAct=0;
	}
	public void agregarElem(Comparable dato){
		if(tamanoAct==tamanoMax){
			tamanoMax=2*tamanoMax;
			Comparable[] copia=elementos;
			elementos=(Comparable[])new Object[tamanoMax];
			for(int i=0;i<tamanoAct;i++){
				elementos[i]=copia[i];
			}
		}

		elementos[tamanoAct] = dato;

		tamanoAct++;
	}

	public Comparable darElementoI(int i) {
		return elementos[i];
	}
	public int darTamAct() {
		return tamanoAct;
	}

	public void anadirElementoPos(int pos, Comparable elem) {
		elementos[pos] = elem;
	}




	public int darTamano(){
		return tamanoAct;
	}
	public Comparable[] darElementos(){

		return elementos;
	}
	public void mergeSort() {
		Comparable[] aux = new Comparable[tamanoAct];
		dividir(aux, 0, tamanoAct -1);
	}
	public void mergeSortC4() {
		Comparable[] aux = new Comparable[tamanoAct];
		dividirC4(aux, 0, tamanoAct-1);
	}
	public void dividirC4(Comparable[] aux, int menor, int mayor) {
		if(mayor <=menor) return;
		int mid = menor + (mayor-menor)/2;
		dividirC4(aux, menor, mid);
		dividirC4(aux, mid+1,mayor);
		juntarC4(aux, menor, mid, mayor);
	}
	public void juntarC4(Comparable[] aux, int menor, int mid, int mayor) {
		for(int i = menor;i<=mayor;i++) {
			aux[i] = elementos[i];
		}
		int i = menor;
		int j = mid+1;
		for(int k = menor;k<=mayor;k++) {
			Trip thiss = (Trip) elementos[j];
			Trip that = (Trip) elementos[i];
			if(i>mid) {
				elementos[k] = aux[j++];
			}
			else if(j>mayor) {
				elementos[k] = aux[i++];
			}
			else if(thiss.compareTo2(that)<0) {
				elementos[k] = aux[j++];
			}
			else {
				elementos[k] = aux[i++];
			}
		}
	}
	
	public void dividir(Comparable[] aux, int menor, int mayor) {
		if(mayor <=menor) return;
		int mid = menor + (mayor-menor)/2;
		dividir(aux, menor, mid);
		dividir(aux, mid+1,mayor);
		juntar(aux, menor, mid, mayor);
	}
	public void juntar(Comparable[] aux, int menor, int mid, int mayor) {
		for(int i = menor;i<=mayor;i++) {
			aux[i] = elementos[i];
		}
		int i = menor;
		int j = mid+1;
		for(int k = menor;k<=mayor;k++) {
			if(i>mid) {
				elementos[k] = aux[j++];
			}
			else if(j>mayor) {
				elementos[k] = aux[i++];
			}
			else if(aux[j].compareTo(aux[i])>0) {
				elementos[k] = aux[j++];
			}
			else {
				elementos[k] = aux[i++];
			}
		}
	}
	

}
